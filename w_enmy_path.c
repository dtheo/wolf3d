/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   w_enmy_path.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtheo <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/19 19:23:59 by dtheo             #+#    #+#             */
/*   Updated: 2018/04/20 18:51:17 by dtheo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void		wolf3d_enmy_path_read(t_ennemy *e, char *buffer)
{
	t_vector2	*path;
	int			len;
	char		**points;
	char		**pos;
	int			i;

	if (!(points = ft_strsplit(buffer, ' ')))
		return ;
	len = wolf3d_len(points);
	if (!(path = (t_vector2 *)ft_memalloc(sizeof(t_vector2) * len)))
		return ;
	i = -1;
	while (++i < len)
	{
		if (!(pos = ft_strsplit(points[i], ',')) || wolf3d_len(pos) != 2)
			break ;
		path[i].x = e->s->pos.x + ft_atoi(pos[0]);
		path[i].y = e->s->pos.y - ft_atoi(pos[1]);
		wolf3d_split_del(pos);
	}
	wolf3d_split_del(points);
	e->path = path;
	e->p_len = len;
}

void			wolf3d_enmy_path(t_data *data, char *file)
{
	int			fd;
	char		*buffer;
	t_ennemy	*e;
	int			ret;

	if ((fd = open(file, O_RDONLY)) < 0)
		return ;
	e = data->e;
	ret = 1;
	while (e != NULL && ret > 0)
	{
		if ((ret = get_next_line(fd, &buffer)) > 0)
		{
			wolf3d_enmy_path_read(e, buffer);
			free(buffer);
		}
		e = e->next;
	}
	close(fd);
}

static void		wolf3d_enmy_follow(t_ennemy *e)
{
	t_vector2	dist;

	e->dir.x = e->path[e->p_next].x - e->s->pos.x;
	e->dir.y = e->path[e->p_next].y - e->s->pos.y;
	dist = e->dir;
	if (e->dir.x * e->dir.x >= e->dir.y * e->dir.y)
	{
		e->dir.y /= e->dir.x;
		e->dir.x = (e->dir.x > 0 ? 1 : -1);
	}
	else
	{
		e->dir.x /= e->dir.y;
		e->dir.y = (e->dir.y > 0 ? 1 : -1);
	}
	e->s->pos.x += e->dir.x * 0.03;
	e->s->pos.y += e->dir.y * 0.03;
	if (dist.x * dist.x < 0.01 && dist.y * dist.y < 0.01)
	{
		e->status = 0;
		e->s->pos = e->path[e->p_next];
		e->p_next = (e->p_next < e->p_len - 1 ? e->p_next + 1 : 0);
	}
	else if (e->status < 1 || e->status > 4)
		e->status = 1;
}

static void		wolf3d_enmy_sangle(t_player *p, t_ennemy *e)
{
	t_vector2	d;
	double		diff;
	double		a;
	int			off;

	d.x = p->pos.x - e->s->pos.x;
	d.y = p->pos.y - e->s->pos.y;
	diff = (atan2(d.y, d.x) + PI) / (2 * PI);
	a = (atan2(e->dir.y, e->dir.x) + PI) / (2 * PI);
	off = round((1 - a) * 8);
	if (diff >= 1 / (double)16 && diff < 3 / (double)16)
		off += 1;
	else if (diff >= 3 / (double)16 && diff < 5 / (double)16)
		off += 2;
	else if (diff >= 5 / (double)16 && diff < 7 / (double)16)
		off += 3;
	else if (diff >= 7 / (double)16 && diff < 9 / (double)16)
		off += 4;
	else if (diff >= 9 / (double)16 && diff < 11 / (double)16)
		off += 5;
	else if (diff >= 11 / (double)16 && diff < 13 / (double)16)
		off += 6;
	else if (diff >= 13 / (double)16 && diff < 15 / (double)16)
		off += 7;
	e->s->id = wolf3d_enmy_sid(e->id) + (off % 8);
}

void			wolf3d_enmy_update(t_data *data, t_ennemy *e)
{
	while (e != NULL)
	{
		if (e->path != NULL)
			wolf3d_enmy_follow(e);
		wolf3d_enmy_sangle(data->player, e);
		if (e->status < 5)
			e->s->id += (e->id == 1 && e->status > 0 ?
				(e->status - 1) * 8 : e->status * 8);
		if (e->status >= 1 && e->status < 5)
		{
			if (e->lifetime % 9 == 0)
				e->status++;
			if (e->status > 4)
				e->status = 1;
		}
		e->lifetime++;
		e = e->next;
	}
}
