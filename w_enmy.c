/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   w_enmy.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtheo <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/19 19:25:21 by dtheo             #+#    #+#             */
/*   Updated: 2018/04/19 19:29:06 by dtheo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int				wolf3d_enmy_sid(int id)
{
	if (id < 2)
		return (48 + 49 * id);
	else if (id == 2)
		return (136);
	return (185 + 51 * (id - 3));
}

void			wolf3d_enmy_push(t_data *data, t_ennemy *e)
{
	t_ennemy	*curr;

	if (data->e == NULL)
		data->e = e;
	else
	{
		curr = data->e;
		while (curr->next != NULL)
			curr = curr->next;
		curr->next = e;
	}
}

t_ennemy		*wolf3d_enmy_new(t_sprite *s, int id)
{
	t_ennemy	*e;

	if (!(e = (t_ennemy *)ft_memalloc(sizeof(t_ennemy))))
		return (NULL);
	e->s = s;
	e->id = id;
	s->id = wolf3d_enmy_sid(id);
	e->status = 0;
	e->lifetime = 0;
	e->dir.x = 1;
	e->dir.y = 0;
	e->path = NULL;
	e->p_len = 0;
	e->p_next = 0;
	e->next = NULL;
	return (e);
}

void			wolf3d_enmy_del(t_ennemy *e)
{
	if (e->next != NULL)
		wolf3d_enmy_del(e->next);
	if (e->path != NULL)
		free(e->path);
	free(e);
}
