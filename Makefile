# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dtheo <dtheo@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/02/26 01:44:44 by dtheo             #+#    #+#              #
#    Updated: 2018/04/19 19:27:08 by dtheo            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = wolf3d

LIBFT = ./libft

IFLAGS = -I includes/ -I $(LIBFT)/includes
MLXFLAGS = -lmlx -framework OpenGL -framework Appkit -lpthread
#-fsanitize=address

CC = gcc
CFLAGS = -Werror -Wall -Wextra $(IFLAGS) -c

OBJS = w_main.o w_tools.o w_data.o w_draw.o w_dda.o w_raycast.o w_ihm.o \
	   w_read.o w_sprite.o w_sprite_cast.o w_enmy_path.o w_enmy.o

.PHONY: all clean fclean re

all: $(NAME)

%.o: %.c
	@ $(CC) $(CFLAGS) $<

$(NAME): $(OBJS)
	@ make -C $(LIBFT)
	@ $(CC) $(MLXFLAGS) $(OBJS) $(LIBFT)/libft.a -o $(NAME)
	@ printf "Compiled $(NAME)!\n"

clean:
	@ make -C $(LIBFT) clean
	@ rm -f $(OBJS)
	@ printf "Object files cleaned!\n"

fclean: clean
	@ make -C $(LIBFT) fclean
	@ rm -f $(NAME)
	@ printf "$(NAME) cleaned!\n"

re: fclean all
