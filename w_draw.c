/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   w_draw.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtheo <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/19 19:10:24 by dtheo             #+#    #+#             */
/*   Updated: 2018/04/19 20:37:36 by dtheo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void			wolf3d_hand(t_data *data, char *pixels)
{
	t_vector2	start;
	t_vector2	pos;
	t_vector2	tex_pos;
	char		*pix;

	start.x = W_WIDTH / 2 - W_TSCL / 2 - floor(cos(data->player->hand) * 20);
	start.y = W_HEIGHT;
	start.y -= (W_TSCL - floor(fabs(sin(data->player->hand)) * 10));
	pos = start;
	while ((tex_pos.x = pos.x - start.x) < W_TSCL && pos.x < W_WIDTH)
	{
		while ((tex_pos.y = pos.y - start.y) < W_TSCL && pos.y < W_HEIGHT)
		{
			if (tex_pos.y >= 100)
			{
				pix = wolf3d_pixel(data->tex_blocs, tex_pos.x, tex_pos.y, 527);
				if (!wolf3d_pixel_alpha(pix))
					ft_strncpy(pixels + (int)(pos.x + pos.y * W_WIDTH) * 4,
						pix, 4);
			}
			pos.y++;
		}
		pos.x++;
		pos.y = start.y;
	}
}

static void		wolf3d_floor(char *pixels, int x, int y)
{
	char		gray[4];
	int			i;

	i = -1;
	while (++i < 3)
		gray[i] = 112;
	gray[3] = 0;
	while (y < W_HEIGHT)
	{
		wolf3d_point(pixels + (x + y * W_WIDTH) * 4, gray);
		y++;
	}
}

static int		wolf3d_ceil(char *pixels, int x, int y, int max)
{
	char		gray[4];
	int			i;

	i = -1;
	while (++i < 3)
		gray[i] = 56;
	gray[3] = 0;
	while (y < max && y < W_HEIGHT)
	{
		wolf3d_point(pixels + (x + y * W_WIDTH) * 4, gray);
		y++;
	}
	return (y);
}

static double	wolf3d_col_posx(t_ray *ray)
{
	double		r;

	r = floor(ray->wall_pos * (double)W_TSCL);
	if (ray->side == 0 && ray->dir.x > 0)
		r = W_TSCL - r - 1;
	if (ray->side == 1 && ray->dir.y < 0)
		r = W_TSCL - r - 1;
	return (r);
}

void			wolf3d_col(int x, t_ray *ray, char *pixels, t_data *data)
{
	int			h;
	int			y;
	int			d;
	t_vector2	pos;
	char		*pix;

	h = floor(W_HEIGHT / ray->wall_d);
	pos.x = wolf3d_col_posx(ray);
	y = wolf3d_ceil(pixels, x, 0, data->player->vh - h / 2);
	if (ray->hit == -1)
		y = data->player->vh + h / 2;
	while (y < data->player->vh + h / 2 && y < W_HEIGHT)
	{
		d = y * 256 - data->player->vh * 2 * 128 + h * 128;
		pos.y = ((d * W_TSCL) / h) / 256;
		if (ray->side == 0)
			pix = wolf3d_pixel(data->tex_blocs, pos.x, pos.y,
				(ray->hit - 1) * 2);
		else
			pix = wolf3d_pixel(data->tex_blocs, pos.x, pos.y,
				(ray->hit - 1) * 2 + 1);
		wolf3d_point(pixels + (x + y * W_WIDTH) * 4, pix);
		y++;
	}
	wolf3d_floor(pixels, x, y);
}
