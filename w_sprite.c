/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   w_sprite.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtheo <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/19 19:22:29 by dtheo             #+#    #+#             */
/*   Updated: 2018/04/20 18:30:19 by dtheo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

t_sprite		*wolf3d_sprite_new(int id, double x, double y)
{
	t_sprite	*sprite;

	if (!(sprite = (t_sprite *)ft_memalloc(sizeof(t_sprite))))
		return (NULL);
	sprite->pos.x = x;
	sprite->pos.y = y;
	if ((id >= 1 && id <= 3) || (id == 7 || id == 8) || (id >= 10 && id <= 13))
		sprite->r = 0.4;
	else
		sprite->r = 0;
	sprite->id = id;
	sprite->dist = 0;
	sprite->next = NULL;
	return (sprite);
}

void			wolf3d_sprite_del(t_sprite *sprite)
{
	if (sprite->next != NULL)
		wolf3d_sprite_del(sprite->next);
	free(sprite);
}

static void		wolf3d_sprite_dist(t_sprite *s, t_vector2 pos)
{
	while (s != NULL)
	{
		s->dist = (pos.x - s->pos.x) * (pos.x - s->pos.x);
		s->dist += (pos.y - s->pos.y) * (pos.y - s->pos.y);
		s = s->next;
	}
}

void			wolf3d_sprite_draw(t_data *data, char *pixels)
{
	t_sprite	**sprites;
	int			len;

	if (data->objects == NULL)
		return ;
	wolf3d_sprite_dist(data->objects, data->player->pos);
	if ((len = wolf3d_sprite_array(&sprites, data->objects)) < 0)
		return ;
	wolf3d_sprite_sort(sprites, len);
	wolf3d_scast(sprites, len, data, pixels);
	free(sprites);
}

int				wolf3d_sprite_coll(t_sprite *elmt, t_vector2 pos)
{
	wolf3d_sprite_dist(elmt, pos);
	while (elmt != NULL)
	{
		if (elmt->dist < elmt->r * elmt->r)
			return (1);
		elmt = elmt->next;
	}
	return (0);
}
