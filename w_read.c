/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   w_read.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtheo <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/19 19:14:37 by dtheo             #+#    #+#             */
/*   Updated: 2018/04/20 18:38:11 by dtheo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

size_t			wolf3d_len(char **split)
{
	size_t		r;

	r = 0;
	while (*split)
	{
		r++;
		split++;
	}
	return (r);
}

static int		wolf3d_read_size(char *buffer, t_map *map)
{
	char		**split;
	int			len;
	int			i;

	if (!(split = ft_strsplit(buffer, ' ')) || wolf3d_len(split) != 2)
		return (0);
	map->size.x = ft_atoi(*split) + 2;
	map->size.y = ft_atoi(*(split + 1)) + 2;
	map->spawn.x = -1;
	map->spawn.y = -1;
	if (map->size.x < 3 || map->size.y < 3)
		return (0);
	len = map->size.x * map->size.y;
	if (!(map->grid = (int *)ft_memalloc(sizeof(int) * len)))
		return (0);
	i = -1;
	while (++i < len)
		*(map->grid + i) = -1;
	wolf3d_split_del(split);
	return (1);
}

static int		wolf3d_readm_place(t_data *data, char *n, int x, int y)
{
	int			j;
	t_sprite	*sprite;

	if (ft_strequ(n, W_SPAWN))
	{
		if (data->map->spawn.x != -1)
			return (0);
		data->map->spawn.x = x + 1;
		data->map->spawn.y = y;
	}
	j = (ft_atoi(n) < 0 || ft_atoi(n) > 53 ? 0 : ft_atoi(n));
	*(data->map->grid + (int)((x + 1) + y * data->map->size.x)) = j;
	if ((*n == '.' && (j = ft_atoi(n + 1)) < 48) ||
			(*n == '$' && (j = ft_atoi(n + 1)) < 5))
	{
		sprite = wolf3d_sprite_new(j, x + 1.5, y + 0.5);
		sprite->next = data->objects;
		data->objects = sprite;
	}
	if (*n == '$' && (j = ft_atoi(n + 1)) < 5)
		wolf3d_enmy_push(data, wolf3d_enmy_new(sprite, j));
	return (1);
}

static int		wolf3d_readm(t_data *data, char *buff, t_map *map, int y)
{
	char		**split;
	int			x;

	if (!(split = ft_strsplit(buff, ' ')) ||
			wolf3d_len(split) != (size_t)(map->size.x - 2))
		return (0);
	x = -1;
	while (*(split + ++x))
	{
		if (!(wolf3d_readm_place(data, *(split + x), x, y)))
			return (0);
	}
	wolf3d_split_del(split);
	return (1);
}

t_map			*wolf3d_read(t_data *data, int fd)
{
	t_map		*map;
	int			y;
	char		*buffer;

	if (!(map = (t_map *)ft_memalloc(sizeof(t_map))))
		return (NULL);
	data->map = map;
	if (get_next_line(fd, &buffer) < 1 || wolf3d_read_size(buffer, map) == 0)
		return (NULL);
	free(buffer);
	y = map->size.y - 2;
	while (get_next_line(fd, &buffer) > 0)
	{
		if (y < 0)
			return (NULL);
		if (wolf3d_readm(data, buffer, map, y) == 0)
			return (NULL);
		free(buffer);
		y--;
	}
	if (map->spawn.x == -1)
		return (NULL);
	return (map);
}
