/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   w_data.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtheo <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/19 19:09:19 by dtheo             #+#    #+#             */
/*   Updated: 2018/04/20 17:44:01 by dtheo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static t_texture	*wolf3d_new_texture(void *mlx, char *file)
{
	t_texture	*tex;
	int			w;
	int			h;

	if (!(tex = (t_texture *)ft_memalloc(sizeof(t_texture))))
		return (NULL);
	tex->img = mlx_xpm_file_to_image(mlx, file, &w, &h);
	if (!tex->img)
		return (NULL);
	tex->pix = wolf3d_get_pixels(tex->img);
	tex->w = w;
	tex->h = h;
	return (tex);
}

static t_player		*wolf3d_player_new(t_vector2 spawn)
{
	t_player		*player;

	if (!(player = (t_player *)ft_memalloc(sizeof(t_player))))
		return (NULL);
	player->pos.x = spawn.x + 0.5;
	player->pos.y = spawn.y + 0.5;
	player->dir.x = 1;
	player->dir.y = 0;
	player->plane.x = 0;
	player->plane.y = -0.66;
	player->speed = NORMAL_SPEED;
	player->vh = W_HEIGHT / 2;
	player->hand = 0;
	return (player);
}

void				*wolf3d_error(const char *level, const char *str)
{
	ft_putstr("wolf3d: ");
	ft_putstr(level);
	ft_putstr(": ");
	ft_putendl(str);
	return (NULL);
}

t_data				*wolf3d_data_new(void *mlx, const char *level)
{
	t_data		*data;
	int			fd;
	int			i;

	if (!(data = (t_data *)ft_memalloc(sizeof(t_data))) ||
			(fd = open(level, O_RDONLY)) < 0)
		return (wolf3d_error(level, strerror(errno)));
	data->mlx = mlx;
	data->objects = NULL;
	data->e = NULL;
	if (!(data->map = wolf3d_read(data, fd)))
		return (wolf3d_error(level, "Error while loading map"));
	if (!(data->player = wolf3d_player_new(data->map->spawn)))
		return (wolf3d_error(level, "Error while creating player"));
	if (!(data->tex_blocs = wolf3d_new_texture(data->mlx, W_TFILE)))
		return (wolf3d_error(level, "Error while loading textures"));
	close(fd);
	i = -1;
	while (++i < 5)
		data->keys[i] = 0;
	data->win = mlx_new_window(mlx, W_WIDTH, W_HEIGHT, "Wolf3D");
	return (data);
}

void				wolf3d_data_del(t_data **data)
{
	if (!data || !(*data))
		return ;
	if ((*data)->map != NULL)
	{
		free((*data)->map->grid);
		free((*data)->map);
	}
	if ((*data)->player != NULL)
		free((*data)->player);
	if ((*data)->tex_blocs != NULL)
	{
		mlx_destroy_image((*data)->mlx, (*data)->tex_blocs->img);
		free((*data)->tex_blocs);
	}
	if ((*data)->objects != NULL)
		wolf3d_sprite_del((*data)->objects);
	if ((*data)->e != NULL)
		wolf3d_enmy_del((*data)->e);
	mlx_destroy_window((*data)->mlx, (*data)->win);
	ft_memdel((void **)data);
}
