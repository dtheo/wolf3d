/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   w_raycast.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtheo <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/19 19:12:16 by dtheo             #+#    #+#             */
/*   Updated: 2018/04/19 20:39:24 by dtheo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static t_targs		wolf3d_args(t_data *data, char *pixels, int x)
{
	t_targs		args;

	args.data = data;
	args.pixels = pixels;
	args.x = x;
	return (args);
}

static void			wolf3d_raycast(t_data *data, char *pixels, int x)
{
	double		cam;
	t_vector2	ray_dir;
	t_ray		*ray;

	cam = 2 * x / (double)W_WIDTH - 1;
	ray_dir.x = data->player->dir.x + data->player->plane.x * cam;
	ray_dir.y = data->player->dir.y + data->player->plane.y * cam;
	if (!(ray = wolf3d_dda(data->map, data->player, ray_dir)))
		return ;
	data->zbuff[x] = ray->wall_d;
	wolf3d_col(x, ray, pixels, data);
	free(ray);
}

static void			*wolf3d_cast(void *p_args)
{
	t_targs		*args;
	int			x;

	args = (t_targs *)p_args;
	x = -1;
	while (++x < W_WORK_SIZE)
		wolf3d_raycast(args->data, args->pixels, x + args->x);
	free(args);
	return (NULL);
}

static pthread_t	*wolf3d_draw_threads(t_data *data, char *pixels)
{
	int			x;
	int			ret;
	pthread_t	*threads;
	t_targs		*args;

	if (!(threads = (pthread_t *)ft_memalloc(sizeof(pthread_t) *
			(W_WIDTH / W_WORK_SIZE))))
		return (NULL);
	x = -1;
	while (++x < W_WIDTH / W_WORK_SIZE)
	{
		if (!(args = (t_targs *)ft_memalloc(sizeof(t_targs))))
			return (NULL);
		*args = wolf3d_args(data, pixels, x * W_WORK_SIZE);
		ret = pthread_create(&(threads[x]), NULL, &wolf3d_cast, args);
		if (ret != 0)
			return (NULL);
	}
	return (threads);
}

void				wolf3d_draw(t_data *data)
{
	void		*img;
	char		*pixels;
	pthread_t	*threads;
	int			x;

	img = mlx_new_image(data->mlx, W_WIDTH, W_HEIGHT);
	pixels = wolf3d_get_pixels(img);
	if (!(data->zbuff = (double *)ft_memalloc(sizeof(double) * W_WIDTH)))
		return ;
	if (!(threads = wolf3d_draw_threads(data, pixels)))
		return ;
	x = -1;
	while (++x < W_WIDTH / W_WORK_SIZE)
		pthread_join(threads[x], NULL);
	free(threads);
	wolf3d_sprite_draw(data, pixels);
	wolf3d_hand(data, pixels);
	mlx_put_image_to_window(data->mlx, data->win, img, 0, 0);
	free(data->zbuff);
	mlx_destroy_image(data->mlx, img);
}
