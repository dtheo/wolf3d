/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   w_sprite_cast.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtheo <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/19 19:22:33 by dtheo             #+#    #+#             */
/*   Updated: 2018/04/19 20:45:36 by dtheo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int				wolf3d_sprite_array(t_sprite ***s, t_sprite *obj)
{
	t_sprite	**sprites;
	int			len;
	t_sprite	*curr;
	int			i;

	curr = obj;
	len = 0;
	while (curr != NULL)
	{
		len++;
		curr = curr->next;
	}
	if (!(sprites = (t_sprite **)ft_memalloc(sizeof(t_sprite *) * len)))
		return (-1);
	i = -1;
	while (obj != NULL && ++i < len)
	{
		*(sprites + i) = obj;
		obj = obj->next;
	}
	*s = sprites;
	return (len);
}

void			wolf3d_sprite_sort(t_sprite **sprites, int len)
{
	int			gap;
	double		factor;
	int			sorted;
	int			i;
	t_sprite	*tmp;

	gap = len;
	factor = 1.3;
	sorted = 0;
	while (sorted == 0)
	{
		gap = floor(gap / factor);
		sorted = (gap > 1 ? 0 : 1);
		gap = (gap < 1 ? 1 : gap);
		i = -1;
		while (++i + gap < len)
		{
			if ((*(sprites + i))->dist < (*(sprites + i + gap))->dist)
			{
				tmp = *(sprites + i);
				*(sprites + i) = *(sprites + i + gap);
				*(sprites + i + gap) = tmp;
			}
		}
	}
}

static t_sray	wolf3d_sprite_ray(t_sprite *s, t_player *p)
{
	t_sray		r;

	r.pos.x = s->pos.x - p->pos.x;
	r.pos.y = s->pos.y - p->pos.y;
	r.id = 1 / (p->plane.x * p->dir.y - p->dir.x * p->plane.y);
	r.trans.x = r.id * (p->dir.y * r.pos.x - p->dir.x * r.pos.y);
	r.trans.y = r.id * (-(p->plane.y) * r.pos.x + p->plane.x * r.pos.y);
	r.ss_x = (W_WIDTH / 2) * (1 + r.trans.x / r.trans.y);
	r.s_h = abs((int)(W_HEIGHT / r.trans.y));
	r.draw_start_y = -(r.s_h) / 2 + p->vh;
	if (r.draw_start_y < 0)
		r.draw_start_y = 0;
	r.draw_end_y = r.s_h / 2 + p->vh;
	if (r.draw_end_y >= W_HEIGHT)
		r.draw_end_y = W_HEIGHT - 1;
	r.draw_start_x = -(r.s_h) / 2 + r.ss_x;
	if (r.draw_start_x < 0)
		r.draw_start_x = 0;
	r.draw_end_x = r.s_h / 2 + r.ss_x;
	if (r.draw_end_x >= W_WIDTH)
		r.draw_end_x = W_WIDTH - 1;
	return (r);
}

static void		wolf3d_draw_s(t_sprite *s, t_data *data, char *pix, t_sray r)
{
	t_vector2	p;
	int			t_x;
	int			t_y;
	int			d;
	char		*pi;

	p.x = r.draw_start_x - 1;
	while (++(p.x) < r.draw_end_x)
	{
		if (r.trans.y > 0 && p.x > 0 && p.x < W_WIDTH &&
				(r.trans.y < data->zbuff[(int)(p.x)] || data->keys[4]))
		{
			t_x = (int)(256 * (p.x - (-(r.s_h) / 2 + r.ss_x)) * W_TSCL / r.s_h);
			t_x /= 256;
			p.y = r.draw_start_y - 1;
			while (++(p.y) < r.draw_end_y)
			{
				d = p.y * 256 - data->player->vh * 2 * 128 + r.s_h * 128;
				t_y = ((d * W_TSCL) / r.s_h) / 256;
				pi = wolf3d_pixel(data->tex_blocs, t_x, t_y, s->id + 108);
				if (!wolf3d_pixel_alpha(pi))
					ft_strncpy(pix + (int)(p.x + p.y * W_WIDTH) * 4, pi, 4);
			}
		}
	}
}

void			wolf3d_scast(t_sprite **s, int len, t_data *data, char *pi)
{
	int			i;
	t_sray		r;

	i = -1;
	while (++i < len)
	{
		r = wolf3d_sprite_ray(*(s + i), data->player);
		wolf3d_draw_s(*(s + i), data, pi, r);
	}
}
