/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   w_dda.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtheo <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/19 19:11:11 by dtheo             #+#    #+#             */
/*   Updated: 2018/04/19 20:38:15 by dtheo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void		wolf3d_init_step(t_player *p, t_ray *ray)
{
	if (ray->dir.x < 0)
	{
		ray->step_x = -1;
		ray->side_d.x = (p->pos.x - ray->map_x) * ray->delta_d.x;
	}
	else
	{
		ray->step_x = 1;
		ray->side_d.x = (ray->map_x + 1 - p->pos.x) * ray->delta_d.x;
	}
	if (ray->dir.y < 0)
	{
		ray->step_y = -1;
		ray->side_d.y = (p->pos.y - ray->map_y) * ray->delta_d.y;
	}
	else
	{
		ray->step_y = 1;
		ray->side_d.y = (ray->map_y + 1 - p->pos.y) * ray->delta_d.y;
	}
}

static void		wolf3d_dda_perform(t_map *map, t_ray *ray)
{
	int			index;

	while (ray->hit == 0)
	{
		index = ray->map_x + ray->map_y * map->size.x;
		if (ray->side_d.x < ray->side_d.y)
		{
			ray->side = 0;
			ray->side_d.x += ray->delta_d.x;
			ray->map_x += ray->step_x;
		}
		else
		{
			ray->side = 1;
			ray->side_d.y += ray->delta_d.y;
			ray->map_y += ray->step_y;
		}
		index = ray->map_x + ray->map_y * map->size.x;
		if (*(map->grid + index) != 0)
			ray->hit = *(map->grid + index);
	}
}

t_ray			*wolf3d_dda(t_map *map, t_player *p, t_vector2 rd)
{
	t_ray		*ray;

	if (!(ray = (t_ray *)ft_memalloc(sizeof(t_ray))))
		return (NULL);
	ray->dir = rd;
	ray->map_x = floor(p->pos.x);
	ray->map_y = floor(p->pos.y);
	ray->delta_d.x = fabs(1 / rd.x);
	ray->delta_d.y = fabs(1 / rd.y);
	ray->hit = 0;
	wolf3d_init_step(p, ray);
	wolf3d_dda_perform(map, ray);
	if (ray->side == 0)
	{
		ray->wall_d = (ray->map_x - p->pos.x + (1 - ray->step_x) / 2) / rd.x;
		ray->wall_pos = p->pos.y + ray->wall_d * rd.y;
	}
	else
	{
		ray->wall_d = (ray->map_y - p->pos.y + (1 - ray->step_y) / 2) / rd.y;
		ray->wall_pos = p->pos.x + ray->wall_d * rd.x;
	}
	ray->wall_pos -= floor(ray->wall_pos);
	return (ray);
}
