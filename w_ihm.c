/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   w_ihm.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtheo <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/19 19:13:03 by dtheo             #+#    #+#             */
/*   Updated: 2018/04/19 20:39:52 by dtheo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int				wolf3d_close(void *param)
{
	t_data		*data;

	data = (t_data *)param;
	wolf3d_data_del(&data);
	exit(0);
	return (0);
}

static int		wolf3d_key_test(int keycode, t_data *data)
{
	if (keycode == 13)
		data->keys[0] = 1;
	else if (keycode == 1)
		data->keys[1] = 1;
	else if (keycode == 2)
		data->keys[2] = 1;
	else if (keycode == 0)
		data->keys[3] = 1;
	else if (keycode == 122)
		data->keys[4] = !(data->keys[4]);
	else if (keycode == 256)
		data->keys[5] = 1;
	else
		return (0);
	return (0);
}

int				wolf3d_key_press(int keycode, void *param)
{
	t_data		*data;

	data = (t_data *)param;
	if (keycode == 53)
		wolf3d_close(data);
	else if (keycode == 257)
		data->player->speed = RUN_SPEED;
	else if (wolf3d_key_test(keycode, data))
		wolf3d_draw(data);
	return (0);
}

int				wolf3d_key_release(int keycode, void *param)
{
	t_data		*data;

	data = (t_data *)param;
	if (keycode == 257)
		data->player->speed = NORMAL_SPEED;
	if (keycode == 256)
		data->keys[5] = 0;
	else if (keycode == 13)
		data->keys[0] = 0;
	else if (keycode == 1)
		data->keys[1] = 0;
	else if (keycode == 2)
		data->keys[2] = 0;
	else if (keycode == 0)
		data->keys[3] = 0;
	return (0);
}
