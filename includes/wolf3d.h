/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtheo <dtheo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 01:50:31 by dtheo             #+#    #+#             */
/*   Updated: 2018/04/20 17:59:33 by dtheo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H

# define W_WIDTH 800
# define W_HEIGHT 600

# define W_SPAWN "+"
# define W_WORK_SIZE 10

# define W_TFILE "./textures/wolf.xpm"
# define W_TSCL 256
# define W_TWIDTH 16
# define W_THEIGHT 43

# define PI 3.14159265
# define PI_8 0.39269908169

# define NORMAL_SPEED 0.05
# define SLOW_SPEED 0.025
# define RUN_SPEED 0.1
# define CROUCH_HEIGHT 20

# include <libft.h>
# include <mlx.h>
# include <math.h>
# include <pthread.h>
# include <errno.h>

typedef struct		s_vector2
{
	double			x;
	double			y;
}					t_vector2;

typedef struct		s_texture
{
	void			*img;
	char			*pix;
	int				w;
	int				h;
}					t_texture;

typedef struct		s_sprite
{
	t_vector2		pos;
	double			r;
	int				id;
	double			dist;
	struct s_sprite	*next;
}					t_sprite;

typedef struct		s_ray
{
	t_vector2		dir;
	int				map_x;
	int				map_y;
	t_vector2		side_d;
	t_vector2		delta_d;
	int				step_x;
	int				step_y;
	int				hit;
	int				side;
	double			wall_d;
	double			wall_pos;
}					t_ray;

typedef struct		s_sray
{
	t_vector2		pos;
	double			id;
	t_vector2		trans;
	int				ss_x;
	int				s_h;
	int				draw_start_y;
	int				draw_end_y;
	int				draw_start_x;
	int				draw_end_x;
}					t_sray;

typedef struct		s_ennemy
{
	t_sprite		*s;
	int				id;
	int				status;
	unsigned int	lifetime;
	t_vector2		dir;
	t_vector2		*path;
	int				p_next;
	int				p_len;
	struct s_ennemy	*next;
}					t_ennemy;

typedef struct		s_player
{
	t_vector2		pos;
	t_vector2		dir;
	t_vector2		plane;
	double			speed;
	int				vh;
	double			hand;
}					t_player;

typedef struct		s_map
{
	t_vector2		size;
	t_vector2		spawn;
	int				*grid;
}					t_map;

typedef struct		s_data
{
	void			*mlx;
	void			*win;
	t_map			*map;
	t_player		*player;
	t_texture		*tex_blocs;
	double			*zbuff;
	t_sprite		*objects;
	t_ennemy		*e;
	int				keys[6];
}					t_data;

typedef struct		s_targs
{
	t_data			*data;
	char			*pixels;
	int				x;
}					t_targs;

void				wolf3d_point(char *pixels, char *pix);
void				wolf3d_split_del(char **split);
char				*wolf3d_get_pixels(void *img);
char				*wolf3d_pixel(t_texture *tex, int x, int y, int id);
int					wolf3d_pixel_alpha(char *pi);
t_data				*wolf3d_data_new(void *mlx, const char *level);
void				wolf3d_data_del(t_data **data);
void				wolf3d_hand(t_data *data, char *pixels);
void				wolf3d_col(int x, t_ray *ray, char *pixels, t_data *data);
t_ray				*wolf3d_dda(t_map *map, t_player *p, t_vector2 rd);
void				wolf3d_draw(t_data *data);
int					wolf3d_close(void *param);
int					wolf3d_key_press(int keycode, void *param);
int					wolf3d_key_release(int keycode, void *param);
size_t				wolf3d_len(char **split);
t_map				*wolf3d_read(t_data *data, int fd);
int					wolf3d_sprite_array(t_sprite ***s, t_sprite *obj);
void				wolf3d_sprite_sort(t_sprite **sprites, int len);
t_sprite			*wolf3d_sprite_new(int id, double x, double y);
void				wolf3d_sprite_del(t_sprite *sprite);
void				wolf3d_sprite_draw(t_data *data, char *pixels);
int					wolf3d_sprite_coll(t_sprite *elmt, t_vector2 pos);
void				wolf3d_scast(t_sprite **s, int len, t_data *data, char *pi);
void				wolf3d_enmy_path(t_data *data, char *file);
void				wolf3d_enmy_update(t_data *data, t_ennemy *e);
int					wolf3d_enmy_sid(int id);
void				wolf3d_enmy_push(t_data *data, t_ennemy *e);
t_ennemy			*wolf3d_enmy_new(t_sprite *s, int id);
void				wolf3d_enmy_del(t_ennemy *e);

#endif
