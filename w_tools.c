/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   w_tools.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtheo <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/19 19:04:55 by dtheo             #+#    #+#             */
/*   Updated: 2018/04/20 17:33:14 by dtheo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void			wolf3d_point(char *pixels, char *pix)
{
	int			i;

	i = -1;
	if (!pixels || !pix)
		return ;
	while (++i < 3 && (pixels + i) != NULL && (pix + i) != NULL)
		*(pixels + i) = *(pix + i);
}

void			wolf3d_split_del(char **split)
{
	int			i;

	i = -1;
	while (*(split + ++i) != NULL)
		free(*(split + i));
	free(split);
}

char			*wolf3d_get_pixels(void *img)
{
	char		*pixels;
	int			bpp;
	int			size;
	int			endian;

	pixels = mlx_get_data_addr(img, &bpp, &size, &endian);
	return (pixels);
}

char			*wolf3d_pixel(t_texture *tex, int x, int y, int id)
{
	char		*pix;
	int			xoff;
	int			yoff;
	int			index;
	int			k;

	k = (tex->w - (W_TWIDTH - 1)) / W_TSCL;
	xoff = (id % k) * W_TSCL + (id % k) * 4;
	yoff = (id / k) * W_TSCL + (id / k) * 4;
	index = ((x + xoff) + (y + yoff) * tex->w) * 4;
	if (index + 3 >= ((tex->w - 1) + (tex->h - 1) * tex->w) * 4 + 4)
		return (NULL);
	pix = tex->pix + index;
	return (pix);
}

int				wolf3d_pixel_alpha(char *pi)
{
	if (((pi[0] != -121 || pi[2] != -103) &&
			(pi[0] != -124 || pi[2] != -124) &&
			(pi[0] != -100 || pi[2] != -100)) || pi[1] != 2)
		return (0);
	return (1);
}
