/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   w_main.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtheo <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/19 19:07:37 by dtheo             #+#    #+#             */
/*   Updated: 2018/04/19 20:35:47 by dtheo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void		wolf3d_move(t_data *data, double speed)
{
	int			index;
	t_vector2	pos;

	pos = data->player->pos;
	index = floor(data->player->pos.x + data->player->dir.x * speed * 4);
	index += floor(data->player->pos.y) * data->map->size.x;
	pos.x += data->player->dir.x * speed;
	if (*(data->map->grid + index) == 0 &&
			!wolf3d_sprite_coll(data->objects, pos))
		data->player->pos.x += data->player->dir.x * speed;
	else
		pos.x -= data->player->dir.x * speed;
	index = floor(data->player->pos.y + data->player->dir.y * speed * 4);
	index *= data->map->size.x;
	index += floor(data->player->pos.x);
	pos.y += data->player->dir.y * speed;
	if (*(data->map->grid + index) == 0 &&
			!wolf3d_sprite_coll(data->objects, pos))
		data->player->pos.y += data->player->dir.y * speed;
	data->player->hand += speed * 1.5;
}

static void		wolf3d_rot(t_player *p, double speed)
{
	double		tmp;

	tmp = p->dir.x;
	p->dir.x = p->dir.x * cos(speed) - p->dir.y * sin(speed);
	p->dir.y = tmp * sin(speed) + p->dir.y * cos(speed);
	tmp = p->plane.x;
	p->plane.x = p->plane.x * cos(speed) - p->plane.y * sin(speed);
	p->plane.y = tmp * sin(speed) + p->plane.y * cos(speed);
}

static void		wolf3d_loop_movement(t_data *data)
{
	if (data->keys[0])
		wolf3d_move(data, data->player->speed);
	if (data->keys[1])
		wolf3d_move(data, -(data->player->speed));
	if (data->keys[2])
		wolf3d_rot(data->player, -0.05);
	if (data->keys[3])
		wolf3d_rot(data->player, 0.05);
	if (data->keys[5] && (data->player->speed > SLOW_SPEED ||
				data->player->vh > W_HEIGHT / 2 - CROUCH_HEIGHT))
	{
		data->player->speed -= (double)(NORMAL_SPEED - SLOW_SPEED) / 5;
		data->player->vh -= CROUCH_HEIGHT / 5;
	}
	else if (!(data->keys[5]) && (data->player->speed < NORMAL_SPEED ||
				data->player->vh < W_HEIGHT / 2))
	{
		data->player->speed += (double)(NORMAL_SPEED - SLOW_SPEED) / 5;
		data->player->vh += CROUCH_HEIGHT / 5;
	}
}

static int		wolf3d_loop(void *param)
{
	t_data		*data;

	data = (t_data *)param;
	wolf3d_enmy_update(data, data->e);
	wolf3d_draw(data);
	wolf3d_loop_movement(data);
	return (0);
}

int				main(int argc, char **argv)
{
	t_data		*data;
	void		*mlx;

	if (argc < 2)
	{
		ft_putendl("usage: wolf3d level_file [paths_file]");
		return (0);
	}
	mlx = mlx_init();
	if (!(data = wolf3d_data_new(mlx, *(argv + 1))))
		return (-1);
	if (argc > 2)
		wolf3d_enmy_path(data, *(argv + 2));
	wolf3d_draw(data);
	mlx_hook(data->win, 2, 0, &wolf3d_key_press, data);
	mlx_hook(data->win, 3, 0, &wolf3d_key_release, data);
	mlx_hook(data->win, 17, 0, &wolf3d_close, data);
	mlx_loop_hook(mlx, &wolf3d_loop, data);
	mlx_loop(mlx);
	return (0);
}
